# Synchronisation_Config

Linux PTP taken from: https://sourceforge.net/projects/linuxptp/files/

Linux testPTP taken from linux Kernel: https://github.com/torvalds/linux/tree/master/tools/testing/selftests/ptp

## Scripts
The repo consist of three key scripts.

`ptp.sh` configures the NIC (interface eth1) to the proper ip address. It also configures pin SDP0 for input (i.e. to receive the timing signal from the CTRIE timing card) and SDP1 to emit a periodic pulse every 1 second (for debugging and timing analysis). Finally, the script executes `ts2phcStart.sh` and `ptp4lStart.sh`

`ts2phcStart.sh` executes the `ts2phc` program from the LinuxPTP project. This synchronises the NIC's clock to the PPS pulses received on SDP0

`ptp4lStart.sh` executes the `ptp4l` program from the LinuxPTP project. It uses the configuration specified in `ptpConfig.cfg`.
